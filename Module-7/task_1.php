<?php

$searchRoot = __DIR__.'\test_search';
$searchName = 'test.txt';
$searchResult = array();

/**
 * @param array $searchResult
 * @param string $dir
 * @param string $nameFile
 */
function searchFile(array &$searchResult, string $dir, string $nameFile)
{
    $array = array_diff(scandir($dir), ['.', '..']);
    foreach ($array as $key => $value) {
        if ($value === $nameFile) {
            $searchResult[] = $dir.'\\'.$value;
        } elseif (is_dir($dir.'\\'.$value)) {
            searchFile($searchResult, $dir.'\\'.$value, $nameFile);
        }
    }
}

searchFile($searchResult, $searchRoot, $searchName);

$searchResult = array_filter($searchResult, 'filesize');

if (isset($searchResult)) {
    foreach ($searchResult as $value) {
        echo 'Данный фаил содержит запись : ' .$value.PHP_EOL;
    }
} else {
    echo 'Поиск не дал результатов';
}
