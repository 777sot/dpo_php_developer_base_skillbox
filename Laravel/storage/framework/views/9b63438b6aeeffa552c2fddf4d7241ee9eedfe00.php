<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="<?php echo e(\Illuminate\Support\Facades\URL::asset('css/style.css')); ?>" rel="stylesheet">
    <title>HTML FORM USERS</title>
</head>
<body>
<?php if(!empty($textList)): ?>
    <?php
        $result = json_encode($textList, JSON_THROW_ON_ERROR);
        $output = json_decode($result, true, 512, JSON_THROW_ON_ERROR);
    ?>
    <div class="containerForm">
        <form  method="post" action="" class="row g-3">
            <?php echo csrf_field(); ?>
            <div class="input-group mb-3">
                <?php $__currentLoopData = $output; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i => $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($i === 'id'): ?>
                        <?php
                            $id = $v;
                        ?>
                    <?php elseif($i === 'author'): ?>
                        <div class="col-md-2">
                            <div>Автор : <?php echo e($v); ?></div>
                        </div>
                    <?php elseif($i === 'title'): ?>
                        <div class="col-md-7">
                            <div><h1><?php echo e($v); ?></h1></div>
                        </div>
                    <?php elseif($i === 'date_created'): ?>
                        <div class="col-mb-3">
                            <div>Дата : <?php echo e($v); ?></div>
                        </div>
                    <?php elseif($i === 'text'): ?>
                        <div class="col-md-10">
                            <div class="text"><?php echo e($v); ?></div>
                        </div>
                        <div class="input-group mb-3">
                            <button type="submit" class="btn btn-outline-secondary" name="_method" value="GET"><a class="stretched-link" href="<?php echo e(route('edit-text',[$id])); ?>">Edit</a></button>
                            <button type="submit" class="btn btn-outline-secondary" name="_method" value="GET"><a class="stretched-link" href="<?php echo e(route('list-text')); ?>">List</a></button>
                            <button type="submit" class="btn btn-outline-secondary" name="_method" value="DELETE"><a class="stretched-link" href="<?php echo e(route('delete-text', [$id])); ?>">Delete</a></button>
                        </div>
                    <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </form>
    </div>
<?php endif; ?>
</body>
</html>
<?php /**PATH C:\OpenServer\domains\laravel\resources\views/get_texts_to_read.blade.php ENDPATH**/ ?>