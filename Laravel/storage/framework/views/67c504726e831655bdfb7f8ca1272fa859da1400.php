<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="<?php echo e(\Illuminate\Support\Facades\URL::asset('css/style.css')); ?>" rel="stylesheet">
    <title>HTML FORM USERS</title>
</head>
<body>
<?php use App\Models\GetTextsToEdit; ?>
<div class="containerForm">
    <div class="col-md-7"><h3>Список статей :</h3></div>
    <?php if(!empty($textList)): ?>
         <?php
             $result = json_encode($textList, JSON_THROW_ON_ERROR);
             $output = json_decode($result, true, 512, JSON_THROW_ON_ERROR);
         ?>
         <?php $__currentLoopData = $output; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
             <form  method="post" action="" class="row g-3">
                 <?php echo csrf_field(); ?>
                 <div class="input-group mb-5">
                     <?php $__currentLoopData = $value; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i => $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                         <?php if($i === 'id'): ?>
                            <?php
                                $id = $v
                            ?>
                                <input class="form-control" type="text" name="id" value="<?php echo e($v); ?>" readonly="" hidden>
                         <?php elseif($i === 'author'): ?>
                            <div class="col-md-2">
                                <input class="form-control" type="text" name="author" value="<?php echo e($v); ?>" placeholder="<?php echo e($v); ?>" readonly="">
                            </div>
                         <?php elseif($i === 'title'): ?>
                            <div class="col-md-4">
                                <input class="form-control" type="text" name="title" value="<?php echo e($v); ?>" placeholder="<?php echo e($v); ?>" readonly="">
                            </div>
                         <?php elseif($i === 'date_created'): ?>
                            <div class="col-auto">
                                <input class="form-control" type="text" name="date_created" value="<?php echo e($v); ?>" readonly="">
                            </div>
                            <button type="submit" class="btn btn-outline-secondary" name="_method" value="GET"><a class="stretched-link" href="<?php echo e(route('read-text', [$id])); ?>">Red</a></button>
                            <button type="submit" class="btn btn-outline-secondary" name="_method" value="GET"><a class="stretched-link" href="<?php echo e(route('edit-text',[$id])); ?>">Edit</a></button>
                            <button type="submit" class="btn btn-outline-secondary" name="_method" value="DELETE"><a class="stretched-link" href="<?php echo e(route('delete-text', [$id])); ?>">Delete</a></button>
                         <?php endif; ?>
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                 </div>
             </form>
         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endif; ?>
</div>

<div class="containerForm">
    <div class="col-md-7"><h3>Добавить новую статью :</h3></div>
    <form  method="post" action="<?php echo e(route('create-text')); ?>" class="row g-3">
        <?php echo csrf_field(); ?>
        <div class="input-group mb-7">
            <div class="col-md-2">
                <input class="form-control" type="text" name="author" value="" placeholder="Author">
            </div>
            <div class="col-md-4">
                <input class="form-control" type="text" name="slug" value="" readonly="" hidden>
                <input class="form-control" type="text" name="title" value="" placeholder="Title">
            </div>
        </div>
        <div class="col-md-6">
             <textarea class="form-control" type="text" name="text" value="" placeholder="Text" rows="5"></textarea>
        </div>
        <div class="col-md-0">
            <button type="submit" class="btn btn-outline-secondary"  name="_method" value="POST">Добавить</button>
        </div>
    </form>
</div>
</body>
</html>
<?php /**PATH C:\OpenServer\domains\laravel\resources\views/get_texts_to_edit.blade.php ENDPATH**/ ?>