<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="<?php echo e(asset('laravel/public/css/style.css')); ?>" rel="stylesheet">
    <title>HTML FORM USERS</title>
</head>
<body>
<?php
    if (!empty($textList)) {
        $result = json_encode($textList, JSON_THROW_ON_ERROR);
        $output = json_decode($result, true, 512, JSON_THROW_ON_ERROR);
    }
?>
<div class="containerForm">
    <div class="col-md-7"><h3>Редактировать статью :</h3></div>
    <form method="post" action="<?php echo e(route('update-text')); ?>" class="row g-3">
        <?php echo csrf_field(); ?>
        <?php echo method_field('PUT'); ?>
        <?php $__currentLoopData = $output; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i => $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php if($i === 'id'): ?>
                <?php
                    $id = $v;
                ?>
                <div class="col-md-0">
                    <input class="form-control" type="text" name="id" value="<?php echo e($v); ?>" readonly="" hidden>
                </div>
                <div class="input-group mb-7">
                    <?php elseif($i === 'author'): ?>
                        <div class="col-md-2">
                            <input class="form-control" type="text" name="author" value="<?php echo e($v); ?>" placeholder="<?php echo e($v); ?>">
                        </div>
                    <?php elseif($i === 'title'): ?>
                        <div class="col-md-4">
                            <input class="form-control" type="text" name="slug" value="<?php echo e($v); ?>" readonly="" hidden>
                            <input class="form-control" type="text" name="title" value="<?php echo e($v); ?>" placeholder="<?php echo e($v); ?>">
                        </div>
                </div>
            <?php elseif($i === 'text'): ?>
                <div class="col-md-6">
                    <textarea class="form-control" type="text" name="text" value="<?php echo e($v); ?>" placeholder="<?php echo e($v); ?>"
                              rows="8"><?php echo e($v); ?></textarea>
                </div>
            <?php elseif($i === 'date_created'): ?>
                <div class="col-md-0">
                    <input class="form-control" type="text" name="date_created" value="<?php echo e($v); ?>" readonly="" hidden>
                </div>
                <div class="input-group mb-3">
                    <button type="submit" class="btn btn-outline-secondary" name="_method" value=""><a
                            class="stretched-link" href="<?php echo e(route('read-text', [$id])); ?>">Red</a></button>
                    <button type="submit" class="btn btn-outline-secondary" name="_method" value="GET"><a
                            class="stretched-link" href="<?php echo e(route('list-text')); ?>">List</a></button>
                    <button type="submit" class="btn btn-outline-secondary" name="_method" value="PUT">Update</button>
                    <button type="submit" class="btn btn-outline-secondary" name="_method" value="DELETE"><a
                            class="stretched-link" href="<?php echo e(route('delete-text', [$id])); ?>">Delete</a></button>
                </div>
            <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </form>
</div>
</body>
</html>
<?php /**PATH C:\OpenServer\domains\telegraph\Laravel\resources\views/get_texts_to_edit_pattern.blade.php ENDPATH**/ ?>