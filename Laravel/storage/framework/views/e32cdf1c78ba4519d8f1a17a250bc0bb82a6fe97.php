<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="<?php echo e(\Illuminate\Support\Facades\URL::asset('css/style.css')); ?>" rel="stylesheet">
    <title>HTML FORM USERS</title>
</head>
<body>
<div class="containerForm">
    <div class="col-md-7"><h3>Список пользователей :</h3></div>
    <?php
        $result = json_encode($users);
        $output = json_decode($result, true);
    ?>
    <?php $__currentLoopData = $output; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <form  method="post" action="" class="row g-3">
            <div class="input-group mb-3">
                <?php $__currentLoopData = $value; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i => $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($i === 'id'): ?>
                        <?php
                            $id = $v;
                        ?>
                        <div class="col-md-0">
                            <input class="form-control" type="text" name="id" value="<?php echo e($v); ?>" readonly="" hidden>
                        </div>
                    <?php elseif($i === 'first_name'): ?>
                        <div class="col-md-0">
                            <input class="form-control" type="text" name="first_name" value="<?php echo e($v); ?>" placeholder="<?php echo e($v); ?>">
                        </div>
                    <?php elseif($i === 'last_name'): ?>
                        <div class="col-md-0">
                            <input class="form-control" type="text" name="last_name" value="<?php echo e($v); ?>" placeholder="<?php echo e($v); ?>">
                        </div>
                    <?php elseif($i === 'age'): ?>
                        <div class="col-md-0">
                            <input class="form-control" type="text" name="age" value="<?php echo e($v); ?>" placeholder="<?php echo e($v); ?>">
                        </div>
                    <?php elseif($i === 'email'): ?>
                        <div class="col-md-0">
                            <input class="form-control" type="text" name="email" value="<?php echo e($v); ?>" placeholder="<?php echo e($v); ?>">
                        </div>
                    <?php elseif($i === 'date_created'): ?>
                        <div class="col-auto">
                            <input class="form-control" type="text" name="date_created" value="<?php echo e($v); ?>" readonly="">
                        </div>
                        <button type="submit" class="btn btn-outline-secondary" name="_method" value="PUT">Edit</button>
                        <button type="submit" class="btn btn-outline-secondary" name="_method" value="DELETE"><a href="<?php echo e(route('users-delete', [$id])); ?>">Delete</a></button>
                    <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </form>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>
<div class="containerForm">
    <div class="col-md-7"><h3>Добавить нового пользователя :</h3></div>
    <form  method="post" action="" class="row g-3">
        <div class="input-group mb-3">
            <div class="col-md-0">
                <input class="form-control" type="text" name="first_name" value="" placeholder="Имя">
            </div>
            <div class="col-md-0">
                <input class="form-control" type="text" name="last_name" value="" placeholder="Фамилия">
            </div>
            <div class="col-md-0">
                <input class="form-control" type="text" name="age" value="" placeholder="Возраст">
            </div>
            <div class="col-md-0">
                <input class="form-control" type="text" name="email" value="" placeholder="e-mail">
            </div>
            <button type="submit" class="btn btn-outline-secondary" value="POST" name="insert">Добавить</button>
        </div>
    </form>
</div>
</body>
</html>
<?php /**PATH C:\OpenServer\domains\laravel\resources\views/users.blade.php ENDPATH**/ ?>