<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\User;
use App\Http\Controllers\GetTextsToEditController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/users', [User::class, 'list']);
Route::post('/users', [User::class, 'create']);
Route::put('/users', [User::class, 'update']);
Route::get('/users/{id}', [User::class, 'delete'])->name('users-delete');

Route::get('/get-text', [GetTextsToEditController::class, 'list'])->name('list-text');
Route::post('/get-text', [GetTextsToEditController::class, 'create'])->name('create-text');
Route::put('/get-text', [GetTextsToEditController::class, 'update'])->name('update-text');
Route::get('/get-text/{id}', [GetTextsToEditController::class, 'delete'])->name('delete-text');
Route::get('/get-text/edit/{id}', [GetTextsToEditController::class, 'listEdit'])->name('edit-text');
Route::get('/get-text/read/{id}', [GetTextsToEditController::class, 'read'])->name('read-text');

