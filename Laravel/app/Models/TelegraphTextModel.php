<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TelegraphTextModel extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'author',
        'title',
        'text',
        'slug',
        'published'
    ];
}
