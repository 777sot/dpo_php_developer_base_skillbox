<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

/**
 * Class User
 * @package App\Http\Controllers
 */
class User extends Controller
{
    /**
     * @param Request $request
     */
    public function create(Request $request)
    {
        $first_name = $request->get('first_name');
        $last_name = $request->get('last_name');
        $age = $request->get('age');
        $email = $request->get('email');

        $user = new \App\Models\User();
        $user->first_name = $first_name;
        $user->last_name = $last_name;
        $user->age = $age;
        $user->email = $email;

        $user->save();
        return new RedirectResponse('/users');
    }

    /**
     * @param Request $request
     */
    public function update(Request $request)
    {
        $id = $request->get('id');
        $first_name = $request->get('first_name');
        $last_name = $request->get('last_name');
        $age = $request->get('age');
        $email = $request->get('email');

        $user = \App\Models\User::where('id', '=', $id)->first();
        $user->id = $id;
        $user->first_name = $first_name;
        $user->last_name = $last_name;
        $user->age = $age;
        $user->email = $email;

        $user->save();
        return new RedirectResponse('/users');
    }

    /**
     * @param $id
     * @return RedirectResponse
     */
    public function delete($id)
    {
        \App\Models\User::where('id', '=', $id)->delete();
        return new RedirectResponse('/users');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function list()
    {
        $users = \App\Models\User::all();
        return view('users', compact('users'));
    }
}
