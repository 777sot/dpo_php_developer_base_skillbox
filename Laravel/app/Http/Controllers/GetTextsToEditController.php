<?php

namespace App\Http\Controllers;

use App\Models\GetTextsToEdit;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

/**
 * Class GetTextsToEditController
 * @package App\Http\Controllers
 */
class GetTextsToEditController extends Controller
{
    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function create(Request $request)
    {
        $author = $request->get('author');
        $title = $request->get('title');
        $text = $request->get('text');
        //$slug = $request->get('slug');

        $textObject = new GetTextsToEdit();
        $textObject->author = $author;
        $textObject->title = $title;
        $textObject->text = $text;
        $textObject->slug = preg_replace('/,\s+/','_', $title) . '/' . date("_d_m_Y");

        $textObject->save();
        return redirect()->route('list-text');
    }

    /**,
     * @param $id
     * @return mixed
     */
    public function read()
    {
        $id = request('id');
        $textList = GetTextsToEdit::where('id', '=', $id)->first();
        return view('get_texts_to_read', compact('textList'));
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function update(Request $request)
    {
        $id = $request->get('id');
        $author = $request->get('author');
        $title = $request->get('title');
        $text = $request->get('text');
        $date_created = $request->get('date_created');
        $slug = $request->get('slug');


        $textObject = GetTextsToEdit::where('id', '=', $id)->first();
        $textObject->id = $id;
        $textObject->author = $author;
        $textObject->title = $title;
        $textObject->text = $text;
        $textObject->slug = $slug;
        $textObject->date_created = $date_created;

        $textObject->save();
        return new RedirectResponse ('/get-text/read/'.$id);
    }

    /**
     * @param $id
     * @return RedirectResponse
     */
    public function delete($id)
    {
        GetTextsToEdit::where('id', '=', $id)->delete();
        return redirect()->route('list-text');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function list()
    {
        $textList = GetTextsToEdit::all();
        return view('get_texts_to_edit', compact('textList'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function listEdit()
    {
        $id = request('id');
        $textList = GetTextsToEdit::where('id', '=', $id)->first();
        return view('get_texts_to_edit_pattern', compact('textList'));
    }
}
