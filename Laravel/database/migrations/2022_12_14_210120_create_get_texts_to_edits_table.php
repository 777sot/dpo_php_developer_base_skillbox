<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateGetTextsToEditsTable
 */
class CreateGetTextsToEditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('get_texts_to_edits', function (Blueprint $table) {
            $table->id();
            $table->string('author');
            $table->string('title');
            $table->longtext('text');
            $table->string('slug');
            $table->timestamp('date_created');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('get_texts_to_edits');
    }
}
