<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="{{ asset('laravel/public/css/style.css') }}" rel="stylesheet">
    <title>HTML FORM USERS</title>
</head>
<body>
@php
    if (!empty($textList)) {
        $result = json_encode($textList, JSON_THROW_ON_ERROR);
        $output = json_decode($result, true, 512, JSON_THROW_ON_ERROR);
    }
@endphp
<div class="containerForm">
    <div class="col-md-7"><h3>Редактировать статью :</h3></div>
    <form method="post" action="{{route('update-text')}}" class="row g-3">
        @csrf
        @method('PUT')
        @foreach ($output as $i => $v)
            @if($i === 'id')
                @php
                    $id = $v;
                @endphp
                <div class="col-md-0">
                    <input class="form-control" type="text" name="id" value="{{$v}}" readonly="" hidden>
                </div>
                <div class="input-group mb-7">
                    @elseif($i === 'author')
                        <div class="col-md-2">
                            <input class="form-control" type="text" name="author" value="{{$v}}" placeholder="{{ $v }}">
                        </div>
                    @elseif ($i === 'title')
                        <div class="col-md-4">
                            <input class="form-control" type="text" name="slug" value="{{$v}}" readonly="" hidden>
                            <input class="form-control" type="text" name="title" value="{{$v}}" placeholder="{{$v}}">
                        </div>
                </div>
            @elseif ($i === 'text')
                <div class="col-md-6">
                    <textarea class="form-control" type="text" name="text" value="{{$v}}" placeholder="{{$v}}"
                              rows="8">{{$v}}</textarea>
                </div>
            @elseif ($i === 'date_created')
                <div class="col-md-0">
                    <input class="form-control" type="text" name="date_created" value="{{$v}}" readonly="" hidden>
                </div>
                <div class="input-group mb-3">
                    <button type="submit" class="btn btn-outline-secondary" name="_method" value=""><a
                            class="stretched-link" href="{{route('read-text', [$id])}}">Red</a></button>
                    <button type="submit" class="btn btn-outline-secondary" name="_method" value="GET"><a
                            class="stretched-link" href="{{route('list-text')}}">List</a></button>
                    <button type="submit" class="btn btn-outline-secondary" name="_method" value="PUT">Update</button>
                    <button type="submit" class="btn btn-outline-secondary" name="_method" value="DELETE"><a
                            class="stretched-link" href="{{route('delete-text', [$id])}}">Delete</a></button>
                </div>
            @endif
        @endforeach
    </form>
</div>
</body>
</html>
