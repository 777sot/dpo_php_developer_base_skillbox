<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="{{ asset('laravel/public/css/style.css') }}" rel="stylesheet">
    <title>HTML FORM USERS</title>
</head>
<body>
<?php use App\Models\GetTextsToEdit; ?>
<div class="containerForm">
    <div class="col-md-7"><h3>Список статей :</h3></div>
    @if (!empty($textList))
        @php
            $result = json_encode($textList, JSON_THROW_ON_ERROR);
            $output = json_decode($result, true, 512, JSON_THROW_ON_ERROR);
        @endphp
        @foreach ($output as $item => $value)
            <form method="post" action="" class="row g-3">
                @csrf
                <div class="input-group mb-5">
                    @foreach ($value as $i => $v)
                        @if ($i === 'id')
                            @php
                                $id = $v
                            @endphp
                            <input class="form-control" type="text" name="id" value="{{$v}}" readonly="" hidden>
                        @elseif ($i === 'author')
                            <div class="col-md-2">
                                <input class="form-control" type="text" name="author" value="{{$v}}"
                                       placeholder="{{$v}}" readonly="">
                            </div>
                        @elseif ($i === 'title')
                            <div class="col-md-4">
                                <input class="form-control" type="text" name="title" value="{{$v}}" placeholder="{{$v}}"
                                       readonly="">
                            </div>
                        @elseif ($i === 'date_created')
                            <div class="col-auto">
                                <input class="form-control" type="text" name="date_created" value="{{$v}}" readonly="">
                            </div>
                            <button type="submit" class="btn btn-outline-secondary" name="_method" value="GET"><a
                                    class="stretched-link" href="{{route('read-text', [$id])}}">Red</a></button>
                            <button type="submit" class="btn btn-outline-secondary" name="_method" value="GET"><a
                                    class="stretched-link" href="{{route('edit-text',[$id])}}">Edit</a></button>
                            <button type="submit" class="btn btn-outline-secondary" name="_method" value="DELETE"><a
                                    class="stretched-link" href="{{route('delete-text', [$id])}}">Delete</a></button>
                        @endif
                    @endforeach
                </div>
            </form>
        @endforeach
    @endif
</div>

<div class="containerForm">
    <div class="col-md-7"><h3>Добавить новую статью :</h3></div>
    <form method="post" action="{{route('create-text')}}" class="row g-3">
        @csrf
        <div class="input-group mb-7">
            <div class="col-md-2">
                <input class="form-control" type="text" name="author" value="" placeholder="Author">
            </div>
            <div class="col-md-4">
                <input class="form-control" type="text" name="slug" value="" readonly="" hidden>
                <input class="form-control" type="text" name="title" value="" placeholder="Title">
            </div>
        </div>
        <div class="col-md-6">
            <textarea class="form-control" type="text" name="text" value="" placeholder="Text" rows="5"></textarea>
        </div>
        <div class="col-md-0">
            <button type="submit" class="btn btn-outline-secondary" name="_method" value="POST">Добавить</button>
        </div>
    </form>
</div>
</body>
</html>
