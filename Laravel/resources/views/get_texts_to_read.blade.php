<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="{{ asset('laravel/public/css/style.css') }}" rel="stylesheet">
    <title>HTML FORM USERS</title>
</head>
<body>
@if (!empty($textList))
    @php
        $result = json_encode($textList, JSON_THROW_ON_ERROR);
        $output = json_decode($result, true, 512, JSON_THROW_ON_ERROR);
    @endphp
    <div class="containerForm">
        <form method="post" action="" class="row g-3">
            @csrf
            <div class="input-group mb-3">
                @foreach ($output as $i => $v)
                    @if ($i === 'id')
                        @php
                            $id = $v;
                        @endphp
                    @elseif ($i === 'author')
                        <div class="col-md-2">
                            <div>Автор : {{$v}}</div>
                        </div>
                    @elseif ($i === 'title')
                        <div class="col-md-7">
                            <div><h1>{{$v}}</h1></div>
                        </div>
                    @elseif ($i === 'date_created')
                        <div class="col-mb-3">
                            <div>Дата : {{$v}}</div>
                        </div>
                    @elseif ($i === 'text')
                        <div class="col-md-10">
                            <div class="text">{{$v}}</div>
                        </div>
                        <div class="input-group mb-3">
                            <button type="submit" class="btn btn-outline-secondary" name="_method" value="GET"><a
                                    class="stretched-link" href="{{route('edit-text',[$id])}}">Edit</a></button>
                            <button type="submit" class="btn btn-outline-secondary" name="_method" value="GET"><a
                                    class="stretched-link" href="{{route('list-text')}}">List</a></button>
                            <button type="submit" class="btn btn-outline-secondary" name="_method" value="DELETE"><a
                                    class="stretched-link" href="{{route('delete-text', [$id])}}">Delete</a></button>
                        </div>
                    @endif
                @endforeach
            </div>
        </form>
    </div>
@endif
</body>
</html>
