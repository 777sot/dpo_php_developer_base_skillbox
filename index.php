<?php

require_once "autoload.php";

/*$Laravel = new TelegraphText();
$Laravel->author = 'А.С Пушкин';
$Laravel->slug = 'arrayStorage.txt';
$Laravel->title = 'Деревня';
$Laravel->text = 'Приветствую тебя, пустынный уголок, Приют спокойствия, трудов и вдохновенья, Где льется дней моих невидимый поток... ';

$getTextsToEdit = new getTextsToEdit();
$url = $getTextsToEdit->create($Laravel);
$res = $getTextsToEdit->list();*/

$date = date('Y-m-d H:i:s');
$users = new User('localhost', 'users', 'root', '');
$dataResult = $users->list();

if (isset($_POST['edit'])) {
    $arrayForm = [
        'id' => $_POST['id'],
        'first_name' => $_POST['first_name'],
        'last_name' => $_POST['last_name'],
        'age' => $_POST['age'],
        'email' => $_POST['email'],
        'date_created' =>  $_POST['date_created']
    ];
    $users->update($arrayForm);
    header("Refresh:0");
}
if (isset($_POST['insert'])) {
    $arrayForm = [
        'first_name' => $_POST['first_name'],
        'last_name' => $_POST['last_name'],
        'age' => $_POST['age'],
        'email' => $_POST['email'],
        'date_created' =>  $date
    ];
    $users->create($arrayForm);
    header("Refresh:0");
}
if (isset($_POST['delete'])) {
    $arrayForm = ['id' => $_POST['id']];
    $users->delete($arrayForm);
    header("Refresh:0");
}
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <title>HTML FORM</title>
</head>
<body>
    <div class="containerForm">
        <?php foreach ($dataResult as $item => $value) {?>
            <form  method="post" action="index.php" class="row g-3">
                <div class="input-group mb-3">
                    <?php foreach ($value as $i => $v) {?>
                        <?php if ($i === 'id') {?>
                            <div class="col-md-0">
                                    <input class="form-control" type="text" name="id" value="<?php echo $v; ?>" readonly="" hidden>
                            </div>
                        <?} elseif ($i === 'first_name') {?>
                            <div class="col-md-0">
                                    <input class="form-control" type="text" name="first_name" value="<?php echo $v; ?>" placeholder="<?php echo $v; ?>">
                            </div>
                        <?} elseif ($i === 'last_name') {?>
                            <div class="col-md-0">
                                    <input class="form-control" type="text" name="last_name" value="<?php echo $v; ?>" placeholder="<?php echo $v; ?>">
                            </div>
                        <?} elseif ($i === 'age') {?>
                            <div class="col-md-0">
                                    <input class="form-control" type="text" name="age" value="<?php echo $v; ?>" placeholder="<?php echo $v; ?>">
                            </div>
                        <?} elseif ($i === 'email') {?>
                            <div class="col-md-0">
                                    <input class="form-control" type="text" name="email" value="<?php echo $v; ?>" placeholder="<?php echo $v; ?>">
                            </div>
                        <?} elseif ($i === 'date_created') {?>
                            <div class="col-auto">
                                    <input class="form-control" type="text" name="date_created" value="<?php echo $v; ?>" readonly="">
                            </div>
                            <button type="submit" class="btn btn-outline-secondary" name="edit">Edit</button>
                            <button type="submit" class="btn btn-outline-secondary" name="delete">Delete</button>
                        <?}?>
                    <?}?>
                </div>
            </form>
        <?}?>
    </div>
    <div class="containerForm">
        <form  method="post" action="index.php" class="row g-3">
            <div class="input-group mb-3">
                <div class="col-md-0">
                    <input class="form-control" type="text" name="first_name" value="" placeholder="Имя">
                </div>
                <div class="col-md-0">
                    <input class="form-control" type="text" name="last_name" value="" placeholder="Фамилия">
                </div>
                <div class="col-md-0">
                    <input class="form-control" type="text" name="age" value="" placeholder="Возраст">
                </div>
                <div class="col-md-0">
                    <input class="form-control" type="text" name="email" value="" placeholder="e-mail">
                </div>
                <button type="submit" class="btn btn-outline-secondary" name="insert">Добавить</button>
            </div>
        </form>
    </div>
</body>
</html>
