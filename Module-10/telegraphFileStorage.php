<?php

interface LoggerInterface
{
    public function logMessage($text);
    public function lastMessages($countMes);
}

interface EventListenerInterface
{
    public function attachEvent ($className, $funName);
    public function detouchEvent($className, $funName);
}

class TelegraphText
{
    public $text;
    public $title;
    public $published;
    public $author;
    public $slug;

    public function __construct(string $author, string $slug)
    {
        $this->author = $author;
        $this->slug = __DIR__ . $slug;
        $this->published = date("m.d.Y H:i:s");
    }

    public function storeText()
    {
        $arrayStorage = [
            'published' => $this->published,
            'author' => $this->author,
            'title' => $this->title,
            'text' => $this->text,
        ];
        file_put_contents($this->slug, serialize($arrayStorage));
        return $arrayStorage;
    }

    public function loadText()
    {
        $arrayStorage = [];
        $arrayStorage = unserialize(file_get_contents($this->slug), $arrayStorage);
        return $arrayStorage['text'];
    }

    public function editText(string $title, string $text)
    {
        $this->title = $title;
        $this->text = $text;
    }
}

abstract class Storage implements LoggerInterface, EventListenerInterface
{
    const PATH = __DIR__.'\\';

    public function attachEvent($className, $funName)
    {
        // TODO: Implement attachEvent() method.
    }

    public function detouchEvent($className, $funName)
    {
        // TODO: Implement detouchEvent() method.
    }

    public function lastMessages($countMes)
    {
        if (file_exists(self::PATH."log.txt")) {
            $countMesArray = file(self::PATH."log.txt", FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES );
            $countArray = count($countMesArray);
            if ($countArray > 1 && $countMes !== $countArray) {
                for ($i = 1; $i <= $countMes; $i++) {
                    $lastCountMes[] = $countMesArray[$countArray - $i];
                    if ($countArray === $i) {
                        return $lastCountMes;
                    }
                }
                return $lastCountMes;
            }
            return $countMesArray;
        } else {
            return false;
        }
    }

    public function logMessage($text)
    {
        if (file_exists(self::PATH."log.txt")) {
            file_put_contents(self::PATH."log.txt", $text."\n", FILE_APPEND);
        } else {
            file_put_contents(self::PATH."log.txt", $text."\n");
        }

    }

    abstract function create($text);

    abstract public function read($slug);

    abstract public function update($slug, $object);

    abstract public function delete($slug);

    abstract public function list();
}

abstract class View
{
    public $storage = [];

    public function __construct($storage)
    {
        if (isset($storage)) {
            $this->storage = $storage;
        } else {
            $this->storage = false;
        }
    }

    abstract function displayTextById($id);

    abstract function displayTextByUrl($url);
}

abstract class User implements EventListenerInterface
{
    public $id;
    public $name;
    public $role;

    public function detouchEvent($className, $funName)
    {
        // TODO: Implement detouchEvent() method.
    }

    public function attachEvent($className, $funName)
    {
        // TODO: Implement attachEvent() method.
    }

    abstract public function getTextsToEdit();
}

class getTextsToEdit extends Storage
{
    public function create($text)
    {
        $filename = $text->slug . date("_d_m_Y");
        $i = 1;

        while (file_exists($filename)) {
            $filename = $text->slug . date("_d_m_Y_") . $i;
            $i++;
        }

        $text->slug = $filename;

        file_put_contents($filename, serialize($text));

        return $filename;
    }

    public function read($slug)
    {
        $this->slug = $slug;
        $arrayStorage = [];

        $arrayStorage = unserialize(file_get_contents($this->slug), $arrayStorage);

        return $arrayStorage['text'];
    }

    public function update($slug, $object)
    {
        $this->slug = $slug;

        if ( file_exists($this->slug)) {
            file_put_contents($this->slug, serialize($object), FILE_USE_INCLUDE_PATH);
        } else {
            return false;
        }
    }

    public function delete($slug)
    {
        $this->slug = $slug;

        file_exists($this->slug) ? unlink($this->slug) : false;
    }

    public function list()
    {
        $arrayStorage = [];
        $dir = scandir(self::PATH);
        foreach ($dir as $item => $value) {
            if (preg_match("/#[0-9]{2}\z|[0-9]{1}\z|.txt#/", $value) === 1) {
                $arrayStorage [$item] = unserialize(file_get_contents(self::PATH.$value), $arrayStorage);
            }
        }
        return $arrayStorage ;
    }
}

//$Laravel = new TelegraphText("А.С Пушкин", "\arrayStorage.txt");
//$Laravel->editText("Деревня", "Приветствую тебя, пустынный уголок, Приют спокойствия, трудов и вдохновенья, Где льется дней моих невидимый поток... ");
//$Laravel->storeText();

//$getTextsToEdit = new getTextsToEdit();
//$url = $getTextsToEdit->create($Laravel);
//$res = $getTextsToEdit->list();
//$getTextsToEdit->delete();
//$getTextsToEdit->update();
// $getTextsToEdit->read();
/*(new getTextsToEdit)->logMessage("ERROR_1");
(new getTextsToEdit)->logMessage("ERROR_2");
(new getTextsToEdit)->logMessage("ERROR_3");
(new getTextsToEdit)->logMessage("ERROR_4");
(new getTextsToEdit)->logMessage("ERROR_5");*/

//$ar = (new getTextsToEdit)->lastMessages(2);
//print_r($ar);
