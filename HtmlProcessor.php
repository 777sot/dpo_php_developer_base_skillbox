<?php

header('Content-Type: application/json');
if (isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] === 'POST') {
    $result = preg_replace('/(<meta([^>]+)>)/m', '', file_get_contents('php://input'));
    $result = preg_replace('/(<link([^>]+)>)/m', '', $result);
    $result = preg_replace('/(<iframe([^>]+)\/iframe>)/m', '', $result);
    $result = preg_replace('/(<script([^>]+)\/script>)/m', '', $result);
    $result = preg_replace('/(<style([^>]+)\/style>)/m', '', $result);
    $result = preg_replace('/(<!--([^>]+)-->)/m', '', $result);
    $result = preg_replace('/(<a([^>]+)>)/m', '', $result);
    $result = preg_replace('/(<\/a>)/m', '', $result);
    $result = preg_replace('/(<form([^>]+)>([^>]+)<\/form>)/m', '', $result);
    $result = preg_replace('/(<img([^>]+)>)/m', '', $result);
    if (!empty($result)) {
        $result = json_encode(['formatted_text' => $result], JSON_THROW_ON_ERROR);
        echo $result;
    } else {
        http_response_code(500);
    }
}
