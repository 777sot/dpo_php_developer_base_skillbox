<?php

class TelegraphText
{
    public $text;
    public $title;
    public $published;
    public $author;
    public $slug;

    public function __construct(string $author, string $slug)
    {
        $this->author = $author;
        $this->slug = __DIR__ . $slug;
        $this->published = date("m.d.Y H:i:s");
    }

    public function storeText()
    {
        $arrayStorage = [
            'published' => $this->published,
            'author' => $this->author,
            'title' => $this->title,
            'text' => $this->text,
        ];
        file_put_contents($this->slug, serialize($arrayStorage));
        return $arrayStorage;
    }

    public function loadText()
    {
        $arrayStorage = [];
        $arrayStorage = unserialize(file_get_contents($this->slug), $arrayStorage);
        return $arrayStorage['text'];
    }

    public function editText(string $title, string $text)
    {
        $this->title = $title;
        $this->text = $text;
    }
}

abstract class Storage
{
    abstract function create($text);

    abstract public function read($slug);

    abstract public function update($slug, $object);

    abstract public function delete($slug);

    abstract public function list();
}

abstract class View
{
    public $storage = [];

    public function __construct($storage)
    {
        if (isset($storage)) {
            $this->storage = $storage;
        } else {
            $this->storage = false;
        }
    }

    abstract function displayTextById($id);

    abstract function displayTextByUrl($url);
}

abstract class User
{
    public $id;
    public $name;
    public $role;

    abstract public function getTextsToEdit();
}

class getTextsToEdit extends Storage
{
    const PATH = __DIR__.'\\';

    public function create($text)
    {
        $filename = $text->slug . date("_d_m_Y");
        $i = 1;

        while (file_exists($filename)) {
            $filename = $text->slug . date("_d_m_Y_") . $i;
            $i++;
        }

        $text->slug = $filename;

        file_put_contents($filename, serialize($text));

        return $filename;
    }

    public function read($slug)
    {
        $this->slug = $slug;
        $arrayStorage = [];

        $arrayStorage = unserialize(file_get_contents($this->slug), $arrayStorage);

        return $arrayStorage['text'];
    }

    public function update($slug, $object)
    {
        $this->slug = $slug;

        if ( file_exists($this->slug)) {
            file_put_contents($this->slug, serialize($object), FILE_USE_INCLUDE_PATH);
        } else {
            return false;
        }
    }

    public function delete($slug)
    {
        $this->slug = $slug;

        file_exists($this->slug) ? unlink($this->slug) : false;
    }

    public function list()
    {
        $arrayStorage = [];
        $dir = scandir(self::PATH);
        foreach ($dir as $item => $value) {
            if (preg_match("/#[0-9]{2}\z|[0-9]{1}\z|.txt#/", $value) === 1) {
                $arrayStorage [$item] = unserialize(file_get_contents(self::PATH.$value), $arrayStorage);
            }
        }
        return $arrayStorage ;
    }
}

$telegraph = new TelegraphText("А.С Пушкин", "\arrayStorage.txt");
$telegraph->editText("Деревня", "Приветствую тебя, пустынный уголок, Приют спокойствия, трудов и вдохновенья, Где льется дней моих невидимый поток... ");
$telegraph->storeText();

$getTextsToEdit = new getTextsToEdit();
//$url = $getTextsToEdit->create($Laravel);
//$res = $getTextsToEdit->list();
//$getTextsToEdit->delete();
//$getTextsToEdit->update();
// $getTextsToEdit->read();
