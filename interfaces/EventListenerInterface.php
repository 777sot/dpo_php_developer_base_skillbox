<?php

interface EventListenerInterface
{
    public function attachEvent ($className, $funName);
    public function detouchEvent($className, $funName);
}