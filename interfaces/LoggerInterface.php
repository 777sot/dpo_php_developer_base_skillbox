<?php

interface LoggerInterface
{
    public function logMessage($text);

    /**
     * @param $countMes
     * @return mixed
     */
    public function lastMessages($countMes);
}
