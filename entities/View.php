<?php

abstract class View
{
    public $storage = [];

    public function __construct($storage)
    {
        if (isset($storage)) {
            $this->storage = $storage;
        } else {
            $this->storage = false;
        }
    }

    abstract function displayTextById($id);

    abstract function displayTextByUrl($url);
}