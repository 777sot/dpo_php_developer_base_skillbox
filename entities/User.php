<?php

class User
{
   protected PDO $connection;

    /**
     * User constructor.
     * @param $host
     * @param $dbname
     * @param $user
     * @param $pass
     * @param int $port
     */
    public function __construct($host, $dbname, $user, $pass, $port = 3306)
   {
       try {
           $this->connection = new PDO(
               "mysql:host=$host; dbname=$dbname; port=$port; charset=UTF8",
               $user,
               $pass
           );
       } catch (PDOException $e) {
           echo $e->getMessage();
       }
   }

    /**
     * @param $arrayData
     * @return bool
     */
    function create($arrayData)
   {
       if (!empty($arrayData)) {
           $statement = $this->connection->prepare
           ('INSERT INTO 
                                   users( 
                                         first_name, 
                                         last_name, 
                                         age, 
                                         email,
                                         date_created
                                   ) 
                                   VALUES( 
                                          :first_name, 
                                          :last_name, 
                                          :age, 
                                          :email, 
                                          :date_created
                                   )
           ');
           $statement->execute($arrayData);
       }
       return false;
   }

    /**
     * @param $arrayData
     * @return bool
     */
    function update($arrayData)
   {
       if (!empty($arrayData)) {
           $statement = $this->connection->prepare
           ('UPDATE 
                            users 
                        SET 
                            first_name = :first_name, 
                            last_name = :last_name, 
                            age = :age, 
                            email = :email,
                            date_created = :date_created
                        WHERE 
                             id = :id
           ');
           $statement->execute($arrayData);
       }
       return false;
   }
   function delete($id)
   {
        return $this->connection->prepare('DELETE FROM users WHERE id = :id')->execute($id);
   }
   function list()
   {
       return $this->connection->query('SELECT * FROM users')->fetchAll();
   }
}
