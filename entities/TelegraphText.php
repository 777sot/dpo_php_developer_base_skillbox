<?php

class  strValid extends Exception {}

class TelegraphText
{
    private $text;
    private $title;
    private $published;
    private $author;
    private $slug;

    public function __construct()
    {
        $this->published = date("d_m_Y");
    }

    private function storeText()
    {
        $arrayStorage = [
            'published' => $this->published,
            'author' => $this->author,
            'title' => $this->title,
            'text' => $this->text,
        ];
        file_put_contents($this->slug, serialize($arrayStorage));
    }

    private function loadText()
    {
        $arrayStorage = [];
        $arrayStorage = unserialize(file_get_contents($this->slug), $arrayStorage);
        return $arrayStorage['text'];
    }

    public function editText(string $title, string $text)
    {
        $this->title = $title;
        $this->text = $text;
    }

    static function strTextValid($text)
    {
        if (strlen($text) < 1 || strlen($text) > 500) {
            throw new strValid('Длинна текста статьи не соответствует заданным параметрам');
        }
        return $text;
    }

    public function __set($name, $value)
    {
        if ($name === 'author') {
            if (strlen($value) <= 120) {
                return $this->author = $value;
            }
            return false;
        }
        if ($name === 'title') {
            return $this->title = $value;
        }
        if ($name === 'slug') {
            if (preg_match('/^[.\-_A-Za-z0-9]+$/', $value)) {
                return $this->slug = __DIR__ . '\\'.$value;
            }
            return false;
        }
        if ($name === 'published') {
            if ($value === date("d_m_Y") || $value > date("d_m_Y")) {
                return $this->published = $value;
            }
            return false;
        }
        if ($name === 'text') {
            if (strlen($value) > 0 && strlen($value) < 500) {
                $this->text = $value;
                $this->storeText();
            }
        }
    }

    public function __get($name)
    {
        if ($name === 'author') {
            return $this->author;
        }
        if ($name === 'slug') {
            return  $this->slug;
        }
        if ($name === 'published') {
            return $this->published;
        }
        if ($name === 'text') {
            return $this->loadText();
        }
    }

}