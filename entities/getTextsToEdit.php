<?php

require_once "Storage.php";

class getTextsToEdit extends Storage
{
    public function create($text)
    {
        $filename = $text->slug . date("_d_m_Y");
        $i = 1;

        while (file_exists($filename)) {
            $filename = $text->slug . date("_d_m_Y_") . $i;
            $i++;
        }

        $text->slug = $filename;

        file_put_contents($filename, serialize($text));

        return $filename;
    }

    public function read($slug)
    {
        $this->slug = $slug;
        $arrayStorage = [];

        $arrayStorage = unserialize(file_get_contents($this->slug), $arrayStorage);

        return $arrayStorage['text'];
    }

    public function update($slug, $object)
    {
        $this->slug = $slug;

        if ( file_exists($this->slug)) {
            file_put_contents($this->slug, serialize($object), FILE_USE_INCLUDE_PATH);
        } else {
            return false;
        }
    }

    public function delete($slug)
    {
        $this->slug = $slug;

        file_exists($this->slug) ? unlink($this->slug) : false;
    }

    public function list()
    {
        $arrayStorage = [];
        $dir = scandir(self::PATH);
        foreach ($dir as $item => $value) {
            if (preg_match("/#[0-9]{2}\z|[0-9]{1}\z|.txt#/", $value) === 1) {
                $arrayStorage [$item] = unserialize(file_get_contents(self::PATH.$value), $arrayStorage);
            }
        }
        return $arrayStorage ;
    }
}