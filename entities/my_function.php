<?php

function errorNext($level, $textError, $file, $line)
{
    if ($level) {
        $dt = new \DateTime();
        file_put_contents('admin.log', $dt->format('Y-m-d H:i:s ') . $textError . ' in line ' . $line . ' in file ' . $file . PHP_EOL, FILE_APPEND);
    }
}

function myErrorHandler ()
{
    set_error_handler('errorNext');
    if (isset($_POST['text'])) {
        try {
            TelegraphText::strTextValid($_POST['text']);
        } catch (strValid $exception) {
            echo "<div class='textError'><p>". $exception->getMessage() . '</p></div>';
        }
    }
}