<?php

require_once "interfaces\LoggerInterface.php";
require_once "interfaces\EventListenerInterface.php";

abstract class Storage implements LoggerInterface, EventListenerInterface
{
    const PATH = __DIR__.'\\';

    public function attachEvent($className, $funName)
    {
        // TODO: Implement attachEvent() method.
    }

    public function detouchEvent($className, $funName)
    {
        // TODO: Implement detouchEvent() method.
    }

    public function lastMessages($countMes)
    {
        if (file_exists(self::PATH."log.txt")) {
            $countMesArray = file(self::PATH."log.txt", FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES );
            $countArray = count($countMesArray);
            if ($countArray > 1 && $countMes !== $countArray) {
                for ($i = 1; $i <= $countMes; $i++) {
                    $lastCountMes[] = $countMesArray[$countArray - $i];
                    if ($countArray === $i) {
                        return $lastCountMes;
                    }
                }
                return $lastCountMes;
            }
            return $countMesArray;
        } else {
            return false;
        }
    }

    public function logMessage($text)
    {
        if (file_exists(self::PATH."log.txt")) {
            file_put_contents(self::PATH."log.txt", $text."\n", FILE_APPEND);
        } else {
            file_put_contents(self::PATH."log.txt", $text."\n");
        }

    }

    abstract function create($text);

    abstract public function read($slug);

    abstract public function update($slug, $object);

    abstract public function delete($slug);

    abstract public function list();
}
