<?php

namespace App\Controller;


use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ArticleLikeController extends AbstractController
{
    /**
     * @Route("/articles/{id<\d+>}/like/{type<like|dislike>}", methods={"POST"}, name="app_article_like")
     * @param $id
     * @param $type
     * @param LoggerInterface $logger
     * @return
     * @throws \Exception
     */
    public function like($id, $type, LoggerInterface $logger)
    {
        if ($type === 'like') {
            $likes = random_int(121, 200);
            $logger->info('Кто-то лайкнул статью');
        } else {
            $likes = random_int(0, 119);
            $logger->info('Какая досада, дизлайк');
        }

        return $this->json(['likes' => $likes]);
    }
}
