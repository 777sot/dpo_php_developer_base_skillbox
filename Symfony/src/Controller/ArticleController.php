<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ArticleController
 * @package App\Controller
 */
class ArticleController extends AbstractController
{
    /**
     * @Route("/", name="app_homepage")
     */
    public function homepage()
    {
        return $this->render('articles/homepage.html.twig');
    }

    /**
     * @Route("/articles/{slug}", name="app_article_show")
     * @param $slug
     * @return
     */
    public function show($slug)
    {
        $comments = [
            'Comments - 1',
            'Comments - 2',
            'Comments - 3'
        ];
        return $this->render('articles/show.html.twig', [
            'article' => ucwords(str_replace('-',' ', $slug)),
            'comments' => $comments
        ]);
    }
}