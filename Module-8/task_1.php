<?php

class TelegraphText
{
    public $text;
    public $title;
    public $published;
    public $author;
    public $slug;

    public function __construct(string $author, string $slug)
    {
        $this->author = $author;
        $this->slug = __DIR__ . $slug;
        $this->published = date("m.d.Y H:i:s");
    }

    public function storeText()
    {
        $arrayStorage = [
            'published' => $this->published,
            'author' => $this->author,
            'title' => $this->title,
            'text' => $this->text,
        ];
        file_put_contents($this->slug, serialize($arrayStorage));
        return $arrayStorage;
    }

    public function loadText()
    {
        $arrayStorage = [];
        $arrayStorage = unserialize(file_get_contents($this->slug), $arrayStorage);
        return $arrayStorage['text'];
    }

    public function editText(string $title, string $text)
    {
        $this->title = $title;
        $this->text = $text;
    }
}

$textTelegraph = new TelegraphText("А.С. Пушкин", "\arrayStorage.txt");

$textTelegraph->editText("Зимнее утро", "Мороз и солнце; день чудесный! Еще ты дремлешь, друг прелестный... ");

$textTelegraph->storeText();

echo $textTelegraph->loadText();
