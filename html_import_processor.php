<?php
declare(strict_types=1);

if (isset($_POST['link']) && !empty($_POST['link'])) {
    $jsonFile = [];
    $textCurl = curl_init();
    curl_setopt($textCurl, CURLOPT_URL, $_POST['link']);
    curl_setopt($textCurl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($textCurl, CURLOPT_HTTPGET, true);
    curl_setopt($textCurl, CURLOPT_CUSTOMREQUEST, 'GET');
    $jsonFile = json_encode(['raw_text' => curl_exec($textCurl)], JSON_THROW_ON_ERROR);
    curl_close ($textCurl);
    if (!empty($jsonFile)){
        header_remove();
        $url = $_SERVER['HTTP_HOST']  . '/HtmlProcessor.php';
        $headers = ['Content-Type: application/json'];
        $jsonFile = json_decode($jsonFile, false, 512, JSON_THROW_ON_ERROR);
        $localCurl = curl_init($url);
        curl_setopt($localCurl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($localCurl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($localCurl, CURLOPT_VERBOSE, 1);
        curl_setopt($localCurl, CURLOPT_POSTFIELDS, $jsonFile);
        $result = curl_exec($localCurl);
    }
}
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>FORM LOAD TEXT</title>
    <link rel="stylesheet" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
</head>
<body>
<?php if(!empty($result)) { ?>
<div class="container"><?php print_r(json_decode($result, false, 512, JSON_THROW_ON_ERROR)); ?></div>
<?}?>
<div class="container">
    <form  method="post" action="html_import_processor.php">
        <h2>Загрузка текста со стороннего ресурса</h2>
        <div class="mb-2">
            <textarea name="link"  placeholder="ссылка на русурс" class="form-control textarea"></textarea>
        </div>
        <div class="mb-2">
            <input type="submit" class="btn btn-primary" value="Отправить">
        </div>
    </form>
</div>
</body>
</html>
