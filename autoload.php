<?php

require_once(__DIR__ . 'vendor\autoload.php');
require_once(__DIR__ . 'entities\my_function.php');

/**
 * @param $className
 * @return bool
 */
function my_load($className) {
    $fileName = 'entities/' . $className . '.php';
    if (file_exists($fileName)) {
        require_once $fileName;
    }
    return false;
};

if (spl_autoload_register('my_load') === false) {
    echo 'Ошибка подключения класса' .PHP_EOL;
}
