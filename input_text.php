<?php

require_once "autoload.php";
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <title>HTML FORM</title>
</head>
<body>
    <div class="container">
        <?php
            if (!empty($_POST['author']) && !empty($_POST['title']) && !empty($_POST['text'])) {

                $telegraph = new TelegraphText();
                myErrorHandler ();
                $telegraph->author = $_POST['author'];
                $telegraph->slug = 'arrayStorage.txt';
                $telegraph->title = $_POST['title'];
                $telegraph->text = $_POST['text'];

                $getTextsToEdit = new getTextsToEdit();
                $getTextsToEdit->create($telegraph);

                if ($_POST['email']) {
                    $mail = new PHPMailer(true);
                    $mail->setLanguage('ru', 'vendor/phpmailer/phpmailer/language/');
                    $title = 'Статья : ' . $_POST['title'] . '  Автор : ' . $_POST['author'];
                    $body = "
                        <h3>Новая статья.</h3>
                        <b>Автор :</b>   " . $_POST['author'] . "<br>
                        <b>Заголовок :</b>   " . $_POST['title'] . "
                        <br><br> " . $_POST['text'] . "
                    ";
                    try {
                        $mail->isSMTP();
                        $mail->CharSet = "UTF-8";
                        $mail->SMTPAuth = true;
                        //$mail->SMTPDebug = 1;
                        $mail->Host = 'smtp.mail.ru';
                        $mail->SMTPAuth = true;
                        $mail->Username = '777soten@mail.ru';
                        $mail->Password = 'e1bE4BcYKTfyvQNDLVTV';
                        $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
                        $mail->Port = 465;
                        $mail->setFrom('777soten@mail.ru', 'Система оповещения');
                        $mail->addAddress($_POST['email'], $_POST['author']);
                        //Content
                        $mail->isHTML(true);
                        $mail->Subject = $title;
                        $mail->Body = $body;
                        $mail->send();
                        echo '<div class="msg">Сообщение успешно отправленно</div>' . PHP_EOL;
                    } catch (Exception $e) {
                        echo "<div class='msg_err'>Сообщение не может быть отправленно. <br>{$mail->ErrorInfo}</div> " . PHP_EOL;
                    }
                } else {
                    echo "<div class='msg'> Статья добавлена в архив.<br>Сообщение не было отправленно так как не указан e-mail !!! </div>";
                }
            }
        ?>
        <form  method="post" action="input_text.php">
            <div class="mb-5">
                <input type="text" name="author" placeholder="Введите ваше имя" class="form-control">
            </div>
            <div class="mb-5">
                <input type="email" name="email" placeholder="Введите ваш e-mail" class="form-control">
            </div>
            <div class="mb-5">
                <input type="text" name="title" placeholder="Введите название статьи" class="form-control">
            </div>
            <div class="mb-5">
                <textarea name="text" placeholder="Введите текст статьи..." class="form-control"></textarea>
            </div>
            <div class="mb-5">
                <button type="submit" class="btn btn-primary">Отправить</button>
            </div>
        </form>
    </div>
</body>
</html>
