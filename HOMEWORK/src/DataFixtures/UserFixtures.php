<?php

namespace App\DataFixtures;

use App\Entity\ApiToken;
use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class UserFixtures
 * @package App\DataFixtures
 */
class UserFixtures extends BaseFixtures
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $userPasswordEncoder;

    public function __construct (UserPasswordEncoderInterface $userPasswordEncoder)
    {
        $this -> userPasswordEncoder = $userPasswordEncoder;
    }

    /**
     * @param ObjectManager $manager
     */
    public function loadData (ObjectManager $manager)
    {
        $this->create(User::class, function (User $user) use ($manager) {
            $user
                -> setEmail('admin@symfony.skillbox')
                -> setFirstName('Administrator')
                -> setPassword($this -> userPasswordEncoder -> encodePassword($user, '123456'))
                -> setRoles(['ROLE_ADMIN'])
                -> setIsActive()
                ->setSubscribeToNewsletter('true')
            ;

            $manager->persist(new ApiToken($user));
        });

        $this -> createMany(User::class, 10, function (User $user) use ($manager) {
            $user
                -> setEmail($this -> faker -> email)
                -> setFirstName($this -> faker -> firstName)
                -> setPassword($this -> userPasswordEncoder -> encodePassword($user, '123456'))
                -> setRoles(['ROLE_USER'])
                -> setIsActive()
                ->setSubscribeToNewsletter($this->faker->boolean)
            ;

            $manager->persist(new ApiToken($user));
        });
    }
}
