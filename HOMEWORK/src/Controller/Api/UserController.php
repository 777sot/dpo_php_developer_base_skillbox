<?php

namespace App\Controller\Api;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;


/**
 * Class UserController
 * @package App\Controller\Api
 * @IsGranted("IS_AUTHENTICATED_FULLY")
 */
class UserController extends AbstractController implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @Route("/api/v1/user", name="api_user")
     * @param LoggerInterface $logger
     * @return Response
     */
    public function index(LoggerInterface $logger): Response
    {
        $userJson = $this->json(
            $this->getUser(),
            200,
            [],
            ['groups' => ['main']]
        );
        $logger->info(' User info : ', [$userJson]);

        return  $userJson;

    }
}
