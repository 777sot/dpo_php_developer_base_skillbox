<?php

namespace App\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;


/**
 * Class AdminController
 * @package App\Controller\Admin
 * @IsGranted("ROLE_ADMIN")
 */
class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="app_admin")
     * @param Request $request
     * @param RouterInterface $router
     * @return Response
     */
    public function index(Request $request, RouterInterface $router): Response
    {
        $routes = $router->getRouteCollection()->all();

        foreach ($routes as $value) {
            if ((strpos($value->getPath(), 'admin') !== false) && $request->getPathInfo() !== $value->getPath()) {
                $routesUrl[]= $request->server->get('SYMFONY_DEFAULT_ROUTE_URL') . preg_replace('[/]', '', $value->getPath(), 1) ;
            }
        }

        return $this->render('admin/index.html.twig', [
            'routes' => $routesUrl,
        ]);
    }
}
