<?php

namespace App\Security\Voter;

use App\Entity\Article;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use function assert;

/**
 * Class ArticleVoter
 * @package App\Security\Voter
 */
class ArticleVoter extends Voter
{
    /**
     * @var Security
     */
    private $security;

    /**
     * ArticleVoter constructor.
     * @param Security $security
     */
    public function __construct (Security $security)
    {
        $this -> security = $security;
    }

    protected function supports($attribute, $subject)
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return $attribute === 'MANAGE'
            && $subject instanceof Article;
    }

    /**
     * Perform a single access check operation on a given attribute, subject and token.
     * It is safe to assume that $attribute and $subject already passed the "supports()" method check.
     *
     * @param $attribute
     * @param mixed $subject
     *
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        assert($subject instanceof Article);

        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case 'MANAGE':
                if ($subject->getAuthor() === $user) {
                    return true;
                }
                break;
        }

        if ($this->security->isGranted('ROLE_ADMIN_ARTICLE')) {
            return true;
        }

        return false;
    }
}
