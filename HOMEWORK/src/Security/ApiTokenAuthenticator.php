<?php

namespace App\Security;

use App\Repository\ApiTokenRepository;
use PHPUnit\Util\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

/**
 * Class ApiTokenAuthenticator
 * @package App\Security
 */
class ApiTokenAuthenticator extends AbstractGuardAuthenticator
{
    /**
     * @var ApiTokenRepository
     */
    private $apiTokenRepository;

    /**
     * ApiTokenAuthenticator constructor.
     * @param ApiTokenRepository $apiTokenRepository
     */
    public function __construct (ApiTokenRepository $apiTokenRepository)
    {
        $this -> apiTokenRepository = $apiTokenRepository;
    }

    /**
     * Does the authenticator support the given Request?
     *
     * If this returns false, the authenticator will be skipped.
     *
     * @param Request $request
     * @return bool
     */
    public function supports (Request $request)
    {
        return $request -> headers -> has('Authorization') && strncmp($request -> headers -> get('Authorization'), 'Bearer', 6) === 0;
    }

    /**
     * Get the authentication credentials from the request and return them
     * as any type (e.g. an associate array).
     *
     * Whatever value you return here will be passed to getUser() and checkCredentials()
     *
     * For example, for a form login, you might:
     *
     *      return [
     *          'username' => $request->request->get('_username'),
     *          'password' => $request->request->get('_password'),
     *      ];
     *
     * Or for an API token that's on a header, you might use:
     *
     *      return ['api_key' => $request->headers->get('X-API-TOKEN')];
     *
     * @param Request $request
     * @return mixed Any non-null value
     *
     */
    public function getCredentials (Request $request)
    {
        return substr($request -> headers -> get('Authorization'), 7);
    }

    public function getUser ($credentials, UserProviderInterface $userProvider)
    {
        $token = $this -> apiTokenRepository -> findOneBy(['token' => $credentials]);

        if (!$token) {
            throw new CustomUserMessageAuthenticationException('Invalid token');
        }

        if ($token -> isExpired()) {
            throw new CustomUserMessageAuthenticationException('Token expired');
        }

        return $token -> getUser();
    }

    public function checkCredentials ($credentials, UserInterface $user)
    {
        return true;
    }

    /**
     * Called when authentication executed, but failed (e.g. wrong username password).
     *
     * This should return the Response sent back to the user, like a
     * RedirectResponse to the login page or a 401 response.
     *
     * If you return null, the request will continue, but the user will
     * not be authenticated. This is probably not what you want to do.
     *
     * @param Request $request
     * @param AuthenticationException $exception
     * @return JsonResponse
     */
    public function onAuthenticationFailure (Request $request, AuthenticationException $exception)
    {
        return new JsonResponse([
            'message' => $exception -> getMessage(),
        ], 401);
    }

    public function onAuthenticationSuccess (Request $request, TokenInterface $token, string $providerKey)
    {
    }

    public function start (Request $request, AuthenticationException $authException = null)
    {
        return new Exception('Neva called');
    }

    public function supportsRememberMe ()
    {
        return false;
    }
}
