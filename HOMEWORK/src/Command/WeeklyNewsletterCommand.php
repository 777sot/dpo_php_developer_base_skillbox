<?php

namespace App\Command;

use App\Entity\Article;
use App\Entity\User;
use App\Repository\ArticleRepository;
use App\Repository\UserRepository;
use App\Service\Mailer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use function count;

/**
 * Class WeeklyNewsletterCommand
 * @package App\Command
 */
class WeeklyNewsletterCommand extends Command
{
    protected static $defaultName = 'app:weekly-newsletter';
    protected static $defaultDescription = 'Еженедельная рассылка статей';
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var ArticleRepository
     */
    private $articleRepository;
    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * @param string|null $name The name of the command; passing null means it must be set in configure()
     *
     * @param UserRepository $userRepository
     * @param ArticleRepository $articleRepository
     * @param Mailer $mailer
     */
    public function __construct(
        string $name = null,
        UserRepository $userRepository,
        ArticleRepository $articleRepository,
        Mailer $mailer
    )
    {
        parent::__construct($name);
        $this->userRepository = $userRepository;
        $this->articleRepository = $articleRepository;
        $this->mailer = $mailer;
    }


    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription);
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int 0 if everything went fine, or an exit code
     *
     * @throws TransportExceptionInterface
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @var User $user */
        $users = $this->userRepository->findAllSubscribedUsers();
        /** @var Article[] $article */
        $articles = $this->articleRepository->findAllPublishedLastWeek();

        $io = new SymfonyStyle($input, $output);

        if (count($articles) === 0) {
            $io->warning('За последнюю неделю нет опубликованных статей');
            return 0;
        }

        $io->progressStart(count($users));

        foreach ($users as $user) {

            $this->mailer->sendWeeklyNewsletter($user, $articles);

            $io->progressAdvance();

            sleep(1);

            break;
        }
        $io->progressFinish();
        return 0;
    }
}
