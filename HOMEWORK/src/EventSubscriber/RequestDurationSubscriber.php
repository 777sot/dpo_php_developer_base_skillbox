<?php

namespace App\EventSubscriber;

use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;

class RequestDurationSubscriber implements EventSubscriberInterface
{
    private $startedAt;
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }


    public function startTimer(RequestEvent $event)
    {
        if (! $event->isMainRequest()) {
            return;
        }
        $this->startedAt = microtime(true);
    }

    public function endTimer(ResponseEvent $event)
    {
        if (! $event->isMainRequest()) {
            return;
        }
        $this->logger->info(sprintf('Запрос выполняется %f мс',(microtime(true) - $this->startedAt) * 1000));
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * ['eventName' => 'methodName']
     *  * ['eventName' => ['methodName', $priority]]
     *  * ['eventName' => [['methodName1', $priority], ['methodName2']]]
     *
     * The code must not depend on runtime state as it will only be called at compile time.
     * All logic depending on runtime state must be put into the individual methods handling the events.
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            RequestEvent::class => [
                ['startTimer', 4000],
//              ['action1', 0],
//              ['action2', 0],,
        ],
            ResponseEvent::class => 'endTimer',
        ];
    }
}
