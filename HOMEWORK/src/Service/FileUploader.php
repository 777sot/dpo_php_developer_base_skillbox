<?php


namespace App\Service;


use League\Flysystem\Filesystem;
use League\Flysystem\FilesystemException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\SluggerInterface;
use function is_resource;

/**
 * Class FileUploader
 * @package App\Service
 */
class FileUploader
{

    private SluggerInterface $slugger;

    private Filesystem $articlesFilesystem;

    public function __construct(Filesystem $articlesFilesystem, SluggerInterface $slugger)
    {
        $this->slugger = $slugger;

        $this->articlesFilesystem = $articlesFilesystem;
    }

    /**
     * @param File $file
     * @param string|null $oldFileName
     * @return string
     */
    public function uploadFile(File $file, ?string $oldFileName = null): string
    {
        $fileName = $this->slugger
            ->slug(pathinfo($file instanceof UploadedFile ? $file->getClientOriginalName() : $file->getFilename(), PATHINFO_FILENAME))
            ->append('-' . uniqid('', true))
            ->append('.' . $file->guessExtension())
            ->toString();

        $stream = fopen($file->getPathname(), 'r');

        try {
             $this->articlesFilesystem->writeStream($fileName, $stream);
        } catch (FilesystemException $e) {
        }

        if (is_resource($stream)) {
            fclose($stream);
        }

        try {
            if ($oldFileName && $this->articlesFilesystem->read($oldFileName)) {
                $this->articlesFilesystem->delete($oldFileName);
            }
        } catch (FilesystemException $e) {
        }

//        if (isset($result) && !$result) {
//            throw new \Exception("Ошибка удаления файла: $oldFileName");
//        }

        return $fileName;
    }
}