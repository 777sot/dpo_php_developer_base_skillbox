<?php

namespace App\Service;

use App\Entity\User;
use Closure;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use function count;

class Mailer
{
    /**
     * @var MailerInterface
     */
    private $mailer;

    /**
     * Mailer constructor.
     * @param MailerInterface $mailer
     */
    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param User $user
     */
    public function sendWelcomeMail(User $user)
    {
        $this->send(
            'email/welcome.html.twig',
            'Доро пожаловать на сайт. ',
            $user
        );
    }

    /**
     * @param User $user
     * @param array $articles
     */
    public function sendWeeklyNewsletter(User $user, array $articles)
    {
        $this->send(
            'email/newsletter.html.twig',
            'Еженедельная рассылка новых статей SiteTest',
            $user,
            function (TemplatedEmail $email) use ($articles) {
                $email
                    ->context([
                        'articles' => $articles
                    ])
                    ->attach(
                        'Опубликовано статей на сайте: ' . count($articles),
                        'report_' . date('Y-m-d') . '.txt'
                    );
            }
        );
    }

    private function send(string $template, string $subject, User $user, Closure $callback = null)
    {
        $email = (new TemplatedEmail())
            ->from(new Address('noreply@symfony.skillbox', 'Spill-Coffee-On-The-Keyboard'))
            ->to(new Address($user->getEmail(), $user->getFirstName()))
            ->subject($subject)
            ->htmlTemplate($template);

        if ($callback) {
            $callback($email);
        }
        $this->mailer->send($email);
    }

}
