<?php


namespace App\Form\Model;

use App\Validator\UniqueUser;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class UserRegistrationFormModel
 * @package App\Form\Model
 */
class UserRegistrationFormModel
{
    /**
     * @Assert\NotBlank(message="Поле не должно быть пустым")
     * @Assert\Email()
     * @UniqueUser()
     */
    public $email;

    public $firstName;

    public $plainPassword;

    public $agreeTerms;
}