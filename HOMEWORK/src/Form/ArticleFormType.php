<?php

namespace App\Form;

use App\Entity\Article;
use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * Class ArticleFormType
 * @package App\Form
 */
class ArticleFormType extends AbstractType
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * ArticleFormType constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /**@var Article|null $article */
        $article = $options['data'] ?? null;

        $cannnotEditAuthor = $article && $article->getId() && $article->isPublished();

        $imageConstraints = [
            new Image([
                'allowLandscape' => true,
                'maxSize' => '2M',
                'maxHeight' => '300',
                'maxHeightMessage' => 'Высота картинки должна быть 300 px',
                'maxWidth' => '480',
                'maxWidthMessage' => 'Ширина картинки должна быть 480 px',
                'minHeight' => '300',
                'minHeightMessage' => 'Высота картинки должна быть 300 px',
                'minWidth' => '480',
                'minWidthMessage' => 'Ширина картинки должна быть 480 px',
                'mimeTypes' => 'image/*',
                'mimeTypesMessage' => 'Измените формат картинки',
            ]),
            //new NotBlank(['message' => 'Поле не должно быть пустым, выберите картинку']),
        ];

        if (! $article || $article->getImageFilename()) {
            $imageConstraints[] = new NotNull([
                'message' => 'Не выбранно изображение статьи',
            ]);
        }

        $builder
            ->add('image', FileType::class, [
                'mapped' => false,
                'label' => 'Загрузка картинки',
                'help' => 'Загрузите картинку макс. размер 2 мб',
                'required' => false,
                'constraints' => $imageConstraints,

            ])
            ->add('title', TextType::class, [
                'label' => 'Название статьи',
                'help' => 'Не используйте в названии цифры',
                'required' => false,
            ])
            ->add('description', TextareaType::class, [
                'attr' => ['rows' => 3],
                'label' => 'Описание статьи',
                'post_max_size_message' => 100,
                'required' => false,
            ])
            ->add('body', TextareaType::class, [
                'attr' => ['rows' => 10],
                'label' => 'Содержимое статьи',
                'required' => false,
            ])
            ->add('keywords', TextType::class, [
                'label' => 'Ключевые слова статьи',
                'required' => false
            ])
            ->add('author', EntityType::class, [
                'label' => 'Автор статьи',
                'class' => User::class,
                'choice_label' => function (User $user) {
                    return sprintf('%s (id : %d)', $user->getFirstName(), $user->getId());
                },
                'placeholder' => 'Выберите автора статьи',
                'choices' => $this->userRepository->findAllSortedByName(),
                'invalid_message' => 'Такого автора не существует',
                'required' => false,
                'disabled' => $cannnotEditAuthor,
            ])
        ;

        $builder->get('body')
            ->addModelTransformer(new CallbackTransformer(
                    function ($bodyFromDatabase) {
                        return str_replace('**собака**', 'собака', $bodyFromDatabase);
                    },
                    function ($bodyFromInput) {
                        return str_replace('собака', '**собака**', $bodyFromInput);
                    }
                )
            )
        ;

        if ($options['enable_published_method']) {
            $builder
                ->add('publishedAt', DateType::class, [
                    'label' => 'Дата публикации статьи',
                    'widget' => 'single_text',
                    'invalid_message' => 'Укажите дату, поле не должно быть пустым',
                    'required' => false,
                ])
            ;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
            'enable_published_method' => false,
        ]);
    }
}
