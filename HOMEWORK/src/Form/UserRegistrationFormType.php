<?php

namespace App\Form;

use App\Form\Model\UserRegistrationFormModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\IsTrue;

/**
 * Class UserRegistrationFormType
 * @package App\Form
 */
class UserRegistrationFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'Email',
                'required' => false,
            ])
            ->add('firstName', null, [
                'label' => 'Имя пользователя',
                'required' => false,
            ])
            ->add('plainPassword', PasswordType::class, [
                'label' => 'Пароль',
                'required' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Пароль не указан!'
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Пароль должен быть не меньше 6 символов'
                    ]),
                ],
            ])
            ->add('agreeTerms', CheckboxType::class, [
                'required' => false,
                'constraints' => [
                    new IsTrue([
                        'message' => 'Вы должны согласиться с условиями',
                    ]),
                ],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => UserRegistrationFormModel::class,
        ]);
    }
}
