<?php


namespace App\Form\TypeExtension;


use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class TextareaSizeExtension
 * @package App\Form\TypeExtension
 */
class TextareaSizeExtension extends AbstractTypeExtension
{

    /**
     * Gets the extended types.
     *
     * @return string[]
     */
    public static function getExtendedTypes(): iterable
    {
        return [TextareaType::class];
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        //$view->vars['attr']['rows'] = $options['description']['rows']; // redefine textarea width body form
        //$view->vars['attr']['rows'] = $options['body']['rows'];
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'description' => ['rows' => 3],
            'body' => ['rows' => 10]
        ]);
    }
}