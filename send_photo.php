<?php

session_start();

if (isset($_POST ['form-checker'])) {
    $fileType = $_FILES['file']['type'];
    $fileSize = $_FILES['file']['size'];
    $fileName = $_FILES['file']['name'];
    $rel = $_FILES['file']['tmp_name'];

    $fileSuccessfulLoad = '';
    $fileLimitSize = '';
    $fileLimitFormat = '';
    $fileAlreadyUpload = '';

    if (!isset($_SESSION['file-send-count']) && $_SESSION['file-send-count'] < 1) {
        $_SESSION['file-send-count'] = 0;
    }
    if ($_SESSION['file-send-count'] < 1) {
        if ($fileType === 'image/jpeg' || $fileType === 'image/png') {
            if ($fileSize <= 2097152) {
                try {
                    $fileRel = 'images\\' . $fileName;
                    move_uploaded_file($rel, $fileRel);
                    if (file_exists($fileRel)) {
                        $_SESSION['file-send-count']++;
                        $fileSuccessfulLoad = '<div class=\'msg\'> Фаил успешно загруженq </div>';
                        header('Refresh:2; url=' . $fileRel);
                    }
                } catch (Exception $e) {
                    $dt = new DateTime();
                    file_put_contents('admin.log', $dt->format('Y-m-d H:i:s ') . $e->getMessage() . ' in line ' . $e->getLine() . ' in file ' . $e->getFile() . PHP_EOL, FILE_APPEND);
                }
            } else {
                $fileLimitSize = '<div class=\'msg_err\'> Фаил не должен превышать размкр 2 Мбайта </div>';
            }
        } else {
            $fileLimitFormat = '<div class=\'msg_err\'> Формат файла должен быть jpeg или png </div> ';
        }
    } else {
        $fileAlreadyUpload = '<div class=\'msg_err\'> Фаил уже был вами загружен </div>';
    }
}
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <style>
        .container {
            width: 30%;
            height: 20%;
            margin-top: 5%;
            margin-left: auto;
            margin-right: auto;
            justify-content: center;
            align-items: center;
        }
        .msg {
            color: black;
            background-color: #48b948;
            margin-bottom: 20px;
            padding-left: 5px;
            padding-right: 5px;
            padding-bottom: 10px;
            padding-top: 10px;
            margin-left: auto;
            margin-right: auto;
            text-align: center;
        }
        .msg_err {
            color: black;
            background-color: #ef301d;
            margin-bottom: 20px;
            padding-top: 10px;
            padding-bottom: 20px;
            margin-left: auto;
            margin-right: auto;
            text-align: center;
        }
    </style>
    <title>HTML FORM FILE</title>
</head>
<body>
    <div class="container">
        <? if (! empty($fileSuccessfulLoad)) {
            echo $fileSuccessfulLoad;
        } elseif (! empty($fileLimitSize)) {
            echo $fileLimitSize;
        } elseif (! empty($fileLimitFormat)) {
            echo $fileLimitFormat;
        } elseif (! empty($fileAlreadyUpload)) {
            echo $fileAlreadyUpload;
        }?>
        <form  method="post" action="send_photo.php" enctype="multipart/form-data">
            <input type="hidden" name="form-checker" value="1">
            <div class="mb-2">
                <input type="file" name="file" class="form-control">
            </div>
            <div class="mb-2">
                <button type="submit" class="btn btn-primary">Отправить</button>
            </div>
        </form>
    </div>
</body>
</html>
