<?php

global $textStorage;
$textStorage = array();

/**
 * @param array $textStorage
 * @param string $title
 * @param string $text
 */
function add(array &$textStorage, string $title, string $text)
{
    $textStorage[] = ["title" => $title , "text" => $text];
}

add($textStorage, 'Заголовок_1', 'Текст_1');
add($textStorage, 'Заголовок_2', 'Текст_2');

print_r($textStorage);

function remove(array &$textStorage, int $namKey)
{
    if ($textStorage[$namKey]) {
        unset($textStorage[$namKey]);
        return true;
    }
    return false;
}

var_dump(remove($textStorage, 0));

var_dump(remove($textStorage, 5));

print_r($textStorage);

function edit(array &$textStorage, int $namKey, string  $title = null , string $text = null)
{
    if (isset($textStorage[$namKey]) && $title && $text === null) {
        $textStorage[$namKey]['title'] = $title;
        return true;
    }elseif (isset($textStorage[$namKey]) && $text && $title) {
        $textStorage[$namKey]['title'] = $title;
        $textStorage[$namKey]['text'] = $text;
        return true;
    }elseif (isset($textStorage[$namKey]) && $title === null && $text) {
        $textStorage[$namKey]['text'] = $text;
        return true;
    }
    return false;
}

edit($textStorage, 1, 'Заголовок_2_Отредактированный');

print_r($textStorage);

var_dump(edit($textStorage, 7, 'Заголовок_2_Отредактированный'));
